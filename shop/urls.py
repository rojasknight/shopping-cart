from django.urls import path
from . import views

app_name = 'shop'

urlpatterns = [
    path('', views.AllProducCat, name='allProdCat'),
    path('<slug:c_slug>/', views.AllProducCat, name='products_by_category'),
    path('<slug:c_slug>/<slug:product_slug>/', views.prodCatDetail, name = 'ProdCatDetail')
]
